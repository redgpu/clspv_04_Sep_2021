ods_def<MatmulI8I8I32Op>
implements_interface<LinalgContractionOpInterface> :
def matmul_i8_i8_i32(A: i8(M, K), B: i8(K, N)) -> (C: i32(M, N)) {
  // TODO: ideally something closer to
  //   C(m, n) += cast<i32>(A(m, k)) * cast<i32>(B(k, n))
  C(m, n) = AddIOp<k>(C(m, n), MulIOp(SignExtendIOp32(A(m, k)), SignExtendIOp32(B(k, n))));
}

ods_def<ConvWOp>:
def conv_1d(I: f32(W), K: f32(KW)) -> (O: f32(W)) {
  O(w) = AddFOp<kw>(O(w), MulFOp(I(w + kw), K(kw)));
}

ods_def<ConvHWOp>:
def conv_2d(I: f32(H, W), K: f32(KH, KW)) -> (O: f32(H, W)) {
  O(h, w) = AddFOp<kh, kw>(O(h, w), MulFOp(I(h + kh, w + kw), K(kh, kw)));
}

ods_def<ConvDHWOp>:
def conv_3d(I: f32(D, H, W), K: f32(KD, KH, KW)) -> (O: f32(D, H, W)) {
  O(d, h, w) = AddFOp<kd, kh, kw>(
      O(d, h, w), MulFOp(I(d + kd, h + kh, w + kw), K(kd, kh, kw)));
}

ods_def<DepthwiseConvInputNHWCFilterHWCFOp>:
def depthwise_conv_2d_input_nhwc_filter_hwcf
      (I: f32(N, IH, IW, CI), K: f32(KH, KW, CI, CO))
   -> (O: f32(N, OH, OW, CI, CO))
  attr(strides: 2xi64, dilations: 2xi64)
"""A general depth-wise 2-D convolution operation.

This operation performs depth-wise 2-D convolution over an input `I` and filter
`F` and generates output `O` using the following computation:

```
  O(n, oh, ow, ci, co) = AddFOp<kh, kw>(
      O(n, oh, ow, ci, co),
      MulFOp(I(n, oh * strides[0] + kh * dilations[0], ow * strides[1] + kw * dilations[1], ci),
               K(kh, kw, ci, co)));
```

where

* `I` is a 4-D tensor with shape `(N, IH, IW, CI)`.
* `F` is a 4-D tensor with shape `(KH, KW, CI, CO)`.
* `O` is a 5-D tensor with shape `(N, OH, OW, CI, CO)`.
* `strides` is a 2-element vector attribute for window strides along the
  height/width dimension.

The indexing maps for these three tensors contain 7 dimensions, following the
order of (`N`, `OH`, `OW`, `CI`, `CO`, `KH`, `KW`).

Note: this op only supports any channel multiplier, which is `CO`. To map back
to 4D result as DepthwiseConvInputNHWCFilterHWCOp, you will have to create a
Linalg reshape op which collapses `CI` and `CO` into one dimension.
"""
{
  O(n, oh, ow, ci, co) = AddFOp<kh, kw>(
      O(n, oh, ow, ci, co),
      MulFOp(I(n, oh * strides[0] + kh * dilations[0], ow * strides[1] + kw * dilations[1], ci),
               K(kh, kw, ci, co)));
}

ods_def<DepthwiseConvInputNHWCFilterHWCOp>:
def depthwise_conv_2d_input_nhwc_filter_hwc
      (I: f32(N, IH, IW, C), K: f32(KH, KW, C))
   -> (O: f32(N, OH, OW, C))
  attr(strides: 2xi64, dilations: 2xi64)
"""A depth-wise 2-D convolution operation.

This operation performs depth-wise 2-D convolution over an input `I` and filter
`F` and generates output `O` using the following computation:

```
O(n, oh, ow, c) = AddFOp<kh, kw>(
    O(n, oh, ow, c),
    MulFOp(I(n, oh * strides[0] + kh * dilations[0], ow * strides[1] + kw * dilations[1], c),
             K(kh, kw, c)));
```

where

* `I` is a 4-D tensor with shape `(N, IH, IW, C)`.
* `F` is a 3-D tensor with shape `(KH, KW, C)`.
* `O` is a 4-D tensor with shape `(N, OH, OW, C)`.
* `strides` is a 2-element vector attribute for window strides along the
  height/width dimension.

The indexing maps for these three tensors contain 6 dimensions, following the
order of (`N`, `OH`, `OW`, `C`, `KH`, `KW`).

Note: this op only supports channel multiplier == 1.
"""
{
  O(n, oh, ow, c) = AddFOp<kh, kw>(
      O(n, oh, ow, c),
      MulFOp(I(n, oh * strides[0] + kh * dilations[0], ow * strides[1] + kw * dilations[1], c),
               K(kh, kw, c)));
}

ods_def<ConvInputNWCFilterWCFOp>:
def conv_1d_input_nwc_filter_wcf(I: f32(N, W, C), K: f32(KW, C, F)) -> (O: f32(N, W, F))
  attr(strides: 1xi64, dilations: 1xi64)
""" A 1-D convolution given NWC layout input and WCF layout filter.

Computes a 1-D convolution given 3-D input and filter. The data layout
of input is NWC and the data layout of filter is WCF.

The indexing maps for these three tensors contain 5 dimensions, following the
order of (`N`, `W`, `F`, `KW`, `C`).
"""
{
  O(n, w, f) = AddFOp<kw>(
      O(n, w, f),
      MulFOp(I(n, w * strides[0] + kw * dilations[0], c), K(kw, c, f)));
}

ods_def<ConvInputNHWCFilterHWCFOp>:
def conv_2d_input_nhwc_filter_hwcf(I: f32(N, H, W, C), K: f32(KH, KW, C, F)) -> (O: f32(N, H, W, F))
  attr(strides: 2xi64, dilations: 2xi64)
""" A 2-D convolution given NHWC layout input and HWCF layout filter.

Computes a 2-D convolution given 4-D input and filter. The data layout
of input is NHWC and the data layout of filter is HWCF.

The indexing maps for these three tensors contain 7 dimensions, following the
order of (`N`, `H`, `W`, `F`, `KH`, `KW`, `C`).
"""
{
  O(n, h, w, f) = AddFOp<kh, kw>(
      O(n, h, w, f), MulFOp(I(n, h * strides[0] + kh * dilations[0],
                                w * strides[1] + kw * dilations[1], c),
                              K(kh, kw, c, f)));
}

ods_def<ConvInputNDHWCFilterDHWCFOp>:
def conv_3d_input_ndhwc_filter_dhwcf
    (I: f32(N, D, H, W, C), K: f32(KD, KH, KW, C, F))
  -> (O: f32(N, D, H, W, F))
  attr(strides: 3xi64, dilations: 3xi64)
""" A 3-D convolution given NDHWC layout input and DHWCF layout filter.

Computes a 3-D convolution given 5-D input and filter. The data layout
of input is NDHWC and the data layout of filter is DHWCF.

The indexing maps for these three tensors contain 9 dimensions, following the
order of (`N`, `D`, `H`, `W`, `F`, `KD`, `KH`, `KW`, `C`).
"""
{
  O(n, d, h, w, f) = AddFOp<kd, kh, kw>(
      O(n, d, h, w, f), MulFOp(I(n, d * strides[0] + kd * dilations[0],
                                   h * strides[1] + kh * dilations[1],
                                   w * strides[2] + kw * dilations[2], c),
                                 K(kd, kh, kw, c, f)));
}
